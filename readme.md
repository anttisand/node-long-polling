# Node.js asynchronous communication long polling sample

Long polling is a bit more complicated compared with short polling. In this example, we have a pub/sub event system in place in the server and on random intervals, we emit an event. This event is currently only returning that random wait time, but you could have here any type of an event, perhaps that a new chat message has been added by another client.

The client in really simple, it just makes GET requests to the server and logs any data received. Notice, that sometimes the response is empty. This happens when the transaction times out without any new data. Other times, a new event has been emitted before the timeout, and in that case, you get the event data in the response.

## Installation & running instructions

```npm install``` to pull in the required packages. Express and axios are used in this example.

```node index.js``` will start the server listening on port 3000.

```node client.js``` in another console tab to start the client making long polls to the server.
