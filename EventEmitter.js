//This is a simple pub-sub event system
//You can add any number of listeners for an event
//and when that event is emitted, we simply execute
//all of the listeners registered for that event
//passing the event data for the listener as parameter

class EventEmitter
{
    //Keep track of listeners
    listeners = {};

    //Time to emit the event to all of its listeners
    emit(event)
    {
        //Loop through all of the registered listeners...
        for(let i in this.listeners)
        {
            //Get a ref to the currently looped listener
            let listener = this.listeners[i];
            //Unregister this listener
            this.unregister(i);
            //Execute the listener, pass the given event
            listener(event);
        }
    }

    //Register a new lisner by adding it to an array of listeners
    register(i, listener)
    {
        this.listeners[i] = listener;
        console.log(`Registered a new listener with id ${i}`);
    }

    //Unregister a listener by removing it from the array of listeners
    unregister(i)
    {
        return delete this.listeners[i];
    }
}

//Export this class to be used in the server
module.exports = EventEmitter;
