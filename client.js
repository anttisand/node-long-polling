//It is recommended to use the Fetch API,
//but it was easier to set the timeout with Axios
const axios = require('axios').default;

async function main() {
    while (true) {
        var hadErr = false;
        console.log("requesting...");
        await axios.get('http://localhost:3000', { timeout: 10000 })
            .then(function (response) {
                //Sometimes we will get an empty response. This will happen when the server has reached
                //the timeout value without any new events having taken place.
                //Other times an event has taken place in the server, in which case we would have the
                //event data here as a response.
                console.log(response.data);
            })
            .catch(function (error) {
                console.log('People we have an error!', error);
                hadErr = true;
            });
        if (hadErr) {
            // break out of the loop in case of error
            // maybe in a real live situation we could do something here
            break;
        }
    }
}

main();
