const express = require('express'); //This simplifies handling of requests and responses
const EventEmitter = require('./EventEmitter'); //Our simple event emitter class
const app = express(); //Create a new express server

//Create an instance of the event emitter class
const eventEmitter = new EventEmitter();

//Helper function to halt execution for some time
async function sleep(ms)
{
    return new Promise((resolve) => {
        setTimeout(resolve, ms); //Sleep
    }).catch(function() {});
}

//Fire an event at random intervals
async function main()
{
    while(true)
    {
        //To simulate something taking place in the backend, we simply wait for a random amount of time
        //and then send that wait time as the payload of the response. Here, you might have, for example,
        //a new chat message arriving from another user and then send that as the event.
        const waitTimeMS = Math.floor(Math.random() * 10000);
        await sleep(waitTimeMS);
        //Done waiting for a random amount of time, emit the event.
        eventEmitter.emit({time: waitTimeMS});
    }
}

//Listen for GET requests
app.get('/', (request, response) => {
    //Simple way of making up unique ids for the events
    const id = Date.now().toString();

    //Here we handle new events that have been emitted by the main loop
    const handler = function(event)
    {
        console.log('Event: ', event);
        response.status(201);
        //Send to the client that event payload as JSON
        response.end(JSON.stringify(event));
    };

    //Register the handler to handle the events
    eventEmitter.register(id, handler);

    //The transaction cannot remain open indefinitely
    timer = setTimeout(function() {
        console.log("Timeout");
        const wasUnregistered = eventEmitter.unregister(id);
        console.log("Event was unregisterd", wasUnregistered);
        if(wasUnregistered)
        {
            response.status(200);
            response.end();
        }
    }, 5000);
});

const server = app.listen(3000, () => {
    console.log(`Server is running on http://localhost:3000. Start the client.js on another console tab.`);
    main(); //Start the main app, that will prodice the events
});
